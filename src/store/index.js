import Vue from 'vue';
import Vuex from 'vuex';
// eslint-disable-next-line no-unused-vars
// import createPersistedState from 'vuex-persistedstate';
import indexPage from './modules/indexPage';
import exam from './modules/exam';

Vue.use(Vuex);

// const module01 = {
//   state: {
//     questions: [
//       {
//         status: false,
//         selectq: '',
//         ans: 'A',
//         title: '下列那一個清代臺灣的廳縣，設置最晚？',
//         sel: [
//           { name: '安平縣', value: 'A' },
//           { name: '恆春縣', value: 'B' },
//           { name: '新竹縣', value: 'C' },
//           { name: '淡水廳', value: 'D' },
//         ],
//       },
//       {
//         status: false,
//         selectq: '',
//         ans: 'A',
//         title: '下列那一個城鎮，並非因開港通商後茶葉貿易而興起？',
//         sel: [
//           { name: '鹿谷', value: 'A' },
//           { name: '大稻埕', value: 'B' },
//           { name: '三角湧', value: 'C' },
//           { name: '大嵙崁', value: 'D' },
//         ],
//       },
//       {
//         status: false,
//         selectq: '',
//         ans: 'A',
//         title: '下列那一個清代臺灣的廳縣，設置最晚？',
//         sel: [
//           { name: '安平縣', value: 'A' },
//           { name: '恆春縣', value: 'B' },
//           { name: '新竹縣', value: 'C' },
//           { name: '淡水廳', value: 'D' },
//         ],
//       },
//       {
//         status: false,
//         selectq: '',
//         ans: 'A',
//         title: '下列那一個城鎮，並非因開港通商後茶葉貿易而興起？',
//         sel: [
//           { name: '鹿谷', value: 'A' },
//           { name: '大稻埕', value: 'B' },
//           { name: '三角湧', value: 'C' },
//           { name: '大嵙崁', value: 'D' },
//         ],
//       },
//       {
//         status: false,
//         selectq: '',
//         ans: 'A',
//         title: '下列那一個清代臺灣的廳縣，設置最晚？',
//         sel: [
//           { name: '安平縣', value: 'A' },
//           { name: '恆春縣', value: 'B' },
//           { name: '新竹縣', value: 'C' },
//           { name: '淡水廳', value: 'D' },
//         ],
//       },
//       {
//         status: false,
//         selectq: '',
//         ans: 'A',
//         title: '下列那一個城鎮，並非因開港通商後茶葉貿易而興起？',
//         sel: [
//           { name: '鹿谷', value: 'A' },
//           { name: '大稻埕', value: 'B' },
//           { name: '三角湧', value: 'C' },
//           { name: '大嵙崁', value: 'D' },
//         ],
//       },
//       {
//         status: false,
//         selectq: '',
//         ans: 'A',
//         title: '下列那一個清代臺灣的廳縣，設置最晚？',
//         sel: [
//           { name: '安平縣', value: 'A' },
//           { name: '恆春縣', value: 'B' },
//           { name: '新竹縣', value: 'C' },
//           { name: '淡水廳', value: 'D' },
//         ],
//       },
//       {
//         status: false,
//         selectq: '',
//         ans: 'A',
//         title: '下列那一個城鎮，並非因開港通商後茶葉貿易而興起？',
//         sel: [
//           { name: '鹿谷', value: 'A' },
//           { name: '大稻埕', value: 'B' },
//           { name: '三角湧', value: 'C' },
//           { name: '大嵙崁', value: 'D' },
//         ],
//       },
//       {
//         status: false,
//         selectq: '',
//         ans: 'A',
//         title: '下列那一個清代臺灣的廳縣，設置最晚？',
//         sel: [
//           { name: '安平縣', value: 'A' },
//           { name: '恆春縣', value: 'B' },
//           { name: '新竹縣', value: 'C' },
//           { name: '淡水廳', value: 'D' },
//         ],
//       },
//       {
//         status: false,
//         selectq: '',
//         ans: 'A',
//         title: '下列那一個城鎮，並非因開港通商後茶葉貿易而興起？',
//         sel: [
//           { name: '鹿谷', value: 'A' },
//           { name: '大稻埕', value: 'B' },
//           { name: '三角湧', value: 'C' },
//           { name: '大嵙崁', value: 'D' },
//         ],
//       },
//     ],
//     single: false,
//     finish: false,
//     qkey: 0,
//   },

//   mutations: {
//     checksingle(state, data) {
//       state.single = data;
//     },
//     saveFinish(state, data) {
//       state.finish = data;
//     },
//     setqkey(state, data) {
//       state.qkey = data;
//     },
//     setAns(state, data) {
//       state.questions[data.myindex].selectq = data.myvalue;
//       state.questions[data.myindex].status = data.mystatus;
//     },
//   },
//   actions: {
//     check({ commit }, data) {
//       commit('checksingle', data);
//     },
//     checkFinish({ commit }, data) {
//       commit('saveFinish', data);
//     },
//     writeAns({ commit }, data) {
//       commit('setAns', data);
//     },
//   },
// };

export default new Vuex.Store({
  modules: {
    indexPage,
    exam,
  },
  // plugins: [
  //   createPersistedState(),
  // ],
});
