/* eslint-disable no-new-object */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint no-shadow: ["error", { "allow": ["state"] }] */
import axios from 'axios';
import VueAxios from 'vue-axios';
import que from '@/service/questions';


const state = {
  questions: [],
  selectedAns: {},
  single: false,
  finish: false,
  stopTimes: false,
  qkey: 0,
  score: 0,
  message: '',
};

// actions
const actions = {
  getQuestons({ commit }, data) {
    commit('setQuestions', {});
    console.log(data);
    const paramsData = {
      year: data.year,
      examId: data.examId,
      type: data.type,
      method: data.method,
    };
    const con = que.getQuestions(paramsData);


    con.then((response) => {
      commit('setQuestions', response.data);
      // commit('setAns', { myindex: state.questions.totalQuestion, myvalue: null });
    });

    return con;
  },
  check({ commit }, data) {
    commit('checksingle', data);
  },
  calculationScore({ state, commit }) {
    const finishedQuestions = _.size(state.questions.questionAns);
    const scorePer = 100 / finishedQuestions;
    // console.log(_.differenceWith(state.questions.questionAns, ['A']));
    // const myscore = (finishedQuestions
    // - (_.size(_.difference(state.questions.questionAns, state.selectedAns))))
    // * scorePer;
    let score = 0;
    state.questions.questionAns.forEach((element, index) => {
      const correctAns = element.ans.indexOf(state.selectedAns[index]);
      console.log(correctAns);
      if (correctAns !== -1) {
        score += scorePer;
        commit('setScore', score.toFixed(2));
      } else {
        score += 0;
        commit('setScore', score.toFixed(2));
      }
    });
    // for (let i = 0; i < state.questions.questionAns.length; i += 1) {
    //   const correctAns = state.questions.questionAns[i].ans.indexOf(state.selectedAns[i]);
    //   console.log(correctAns);
    //   if (correctAns !== -1) {
    //     score += scorePer;
    //     commit('setScore', score.toFixed(2));
    //   } else {
    //     score += 0;
    //     commit('setScore', score.toFixed(2));
    //   }
    // }
    // commit('setScore', myscore.toFixed(2));
  },
  checkFinish({ state, commit }) {
    const finishedQuestions = _.size(state.questions.questionAns);
    // console.log(`01：${finishedQuestions}`);
    if (finishedQuestions > _.size(state.selectedAns)) {
      console.log('沒做完');
      // console.log(finishedQuestions);
      commit('saveFinish', {
        tf: false,
        msg: '尚有題目未作答，是否交卷？',
      });
      commit('setTimes', false);
    } else {
      console.log('確定交卷?');
      commit('saveFinish', {
        tf: false,
        msg: '是否交卷？',
      });
      commit('setTimes', false);
    }
    // for (let i = 0; i < state.questions.length; i += 1) {
    //   if (state.questions[i].status !== true) {
    //     alert('沒做完');
    //     commit('saveFinish', true);
    //     commit('setTimes', true);
    //     break;
    //   } else if (state.questions[i].selectq === state.questions[i].ans) {
    //     commit('setScore', 2);
    //     commit('saveFinish', true);
    //     commit('setTimes', true);
    //   } else {
    //     commit('setScore', 0);
    //     commit('saveFinish', true);
    //     commit('setTimes', true);
    //   }
    //   // ↓不判斷是否做完↓
    //   // if (state.questions[i].selectq === state.questions[i].ans) {
    //   //   commit('setScore', 2);
    //   //   commit('saveFinish', true);
    //   //   commit('setTimes', true);
    //   // } else {
    //   //   commit('setScore', 0);
    //   //   commit('saveFinish', true);
    //   //   commit('setTimes', true);
    //   // }
    // }
  },
  writeAns({ commit }, data) {
    commit('setAns', data);
  },
};

// mutations
const mutations = {
  setQuestions(state, data) {
    state.questions = data;
  },
  setTimes(state, data) {
    state.stopTimes = data;
  },
  checksingle(state, data) {
    state.single = data;
  },
  saveFinish(state, data) {
    console.log(data);
    state.finish = data.tf;
    state.message = data.msg;
  },
  setqkey(state, data) {
    state.qkey = data;
  },
  setAns(state, data) {
    // state.questions[data.myindex].selectq = data.myvalue;
    // state.questions[data.myindex].status = data.mystatus;
    state.selectedAns[data.myindex - 1] = data.myvalue;
  },
  clearAns(state) {
    state.selectedAns = {};
  },
  setScore(state, data) {
    state.score = data;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
