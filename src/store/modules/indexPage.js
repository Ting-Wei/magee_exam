/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
/* eslint-disable no-alert */
/* eslint no-shadow: ["error", { "allow": ["state"] }] */
import axios from 'axios';
import VueAxios from 'vue-axios';
import router from '@/router/index';
import mem from '@/service/member';
import que from '@/service/questions';

const state = {
  isLoading_login: false,
  isLogin: false,
  loginInfo: '',
  userinfo_pk: '',
  userinfo: {},
  status: 'test',
  api_token: '',
};

// actions
const actions = {
  getRecord({ state, commit }, data) {
    const postData = {
      id: data.pk,
    };
    const con = mem.QuestionsRecord(postData);
    return con;
  },
  buyquestions({ state, commit }, data) {
    const postData = {
      id: data.id,
      examId: data.examid,
    };
    const con = que.buyquestions(postData);

    con.then((response) => {
      console.log(response.data);
    });
    return con;
  },
  checkLogin({ commit }) {
    commit('changeLoading', true);
    const con = mem.checklogin();
    con.then((response) => {
      // console.log(response.data);
      if (response.data.id) {
        commit('saveUserinfo', response.data);
        commit('isLogin', { TF: true, member_id: 'VIP', loginInfo: '' });
        commit('changeLoading', false);
        // console.log('以登入');
      } else {
        commit('isLogin', { TF: false, member_id: 'test', loginInfo: response.data.info });
        commit('changeLoading', false);
        console.log('未登入');
        // router.push({ to: 'index' });
      }
    });
    return con;
  },
  Login({ commit }, data) {
    commit('changeLoading', true);
    const postData = {
      member_id: data.member_id,
      password: data.password,
    };
    const con = mem.login(postData);
    con.then((response) => {
      // console.log(response.data);
      if (response.data.id) {
        commit('saveUserinfo', response.data);
        commit('loginSetToken', response.data);
        commit('isLogin', { TF: true, member_id: 'VIP' });
        commit('changeLoading', false);
      } else {
        commit('isLogin', { TF: false, member_id: 'test' });
        commit('changeLoading', false);
        console.log('未登入');
      }
    })
      .catch((error) => {
        console.log(data.info, error);
      });
    return con;
  },
};
// mutations
const mutations = {
  changeLoading(state, data) {
    state.isLoading_login = data;
  },
  isLogin(state, data) {
    state.isLogin = data.TF;
    state.status = data.member_id;
    state.loginInfo = data.loginInfo;
  },
  loginSetToken(state, data) {
    localStorage.setItem('api_token', data.api_token);
    state.api_token = localStorage.getItem('api_token');
  },
  saveUserinfo(state, data) {
    state.userinfo_pk = data.id;
    state.userinfo = data;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
};
