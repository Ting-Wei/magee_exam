import Vue from 'vue';
import VueRouter from 'vue-router';
// import Home from '../views/Home.vue';

Vue.use(VueRouter);

let getPage = (name, path = "views") => () => {
  return import(`@/${path}/${name}.vue`);
};

const routes = [
  {
    path: '/',
    name: 'index',
    component: getPage('index'),
    meta:{
      check: true,
    }
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
  {
    path: '/exam/:year/:type/:examId/:method',
    name: 'exam',
    component: getPage('exam'),
    meta:{
      check: true,
    }
    // children: [
    //   {
    //     path: 'fullq',
    //     component: getPage('ComQuestions', 'components'),
    //   },
    //   {
    //     path: 'singleq',
    //     component: getPage('ComQuestions2', 'components'),
    //   },
    // ],
  },
];


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
