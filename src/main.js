import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faUserSecret, faStopwatch, faExclamationTriangle, faTools,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VueScrollTo from 'vue-scrollto';
import VueMoment from 'vue-moment';
import moment from 'moment-timezone';
import _ from 'lodash';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import '@/style/style.scss';

library.add(faUserSecret, faStopwatch, faExclamationTriangle, faTools);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.prototype.axios = axios;
Vue.config.productionTip = false;
Vue.use(VueAxios, axios, VueScrollTo, _, Loading);
Vue.use(VueMoment, {
  moment,
});
Vue.use(VueScrollTo, {
  container: 'body',
  duration: 500,
  easing: 'ease',
  offset: 0,
  force: true,
  cancelable: true,
  onStart: false,
  onDone: false,
  onCancel: false,
  x: false,
  y: true,
});
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
