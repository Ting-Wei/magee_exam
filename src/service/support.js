/* eslint-disable no-param-reassign */
import axios from 'axios';
import _ from 'lodash';


export default {
  get(url, data, config) {
    const params = _.map(data, (item, keys) => `${keys}=${item}`).join('&');

    console.log(params);
    return this.http('get', `${url}?${params}`, config);
  },
  post(url, data, config) {
    return this.http('post', url, data, config);
  },
  http(method, url, response, config = {}) {
    if (config) {
      if (config.headers) {
        config.headers.Authorization = `Bearer ${localStorage.getItem('api_token')}`;
      } else {
        config.headers = { Authorization: `Bearer ${localStorage.getItem('api_token')}` };
      }
    } else {
      config = {
        headers: { Authorization: `Bearer ${localStorage.getItem('api_token')}` },
      };
    }
    // console.log(process.env);
    return axios.request({
      url,

      // `method` is the request method to be used when making the request
      method, // default

      // `baseURL` will be prepended to `url` unless `url` is absolute.
      // It can be convenient to set `baseURL` for an instance of axios to pass relative URLs
      // to methods of that instance.
      baseURL: process.env.VUE_APP_LOCAL,
      data: response,
      // transformRequest: [function (data, headers) {
      //   // Do whatever you want to transform the data
      //   return data;
      // }],
      // `transformResponse` allows changes to the response data to be made before
      // it is passed to then/catch
      // transformResponse: [function (data) {
      //   // Do whatever you want to transform the data
      //   return data;
      // }],
      ...config,
      // `headers` are custom headers to be sent
      // headers: {'X-Requested-With': 'XMLHttpRequest'},
    });
  },
};
