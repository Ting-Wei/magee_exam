// eslint-disable-next-line import/no-named-as-default
import sup from './support';

export default {
  checklogin() {
    return sup.post('login');
  },
  login(data) {
    return sup.post('login', data);
  },
  QuestionsRecord(data) {
    return sup.post('QuestionsRecord', data);
  },
};
