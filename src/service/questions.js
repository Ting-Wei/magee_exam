// eslint-disable-next-line import/no-named-as-default
import sup from './support';

export default {
  getQuestions(data) {
    return sup.get('getQuestions', data);
  },
  buyquestions(data) {
    return sup.post('buy', data);
  },
};
